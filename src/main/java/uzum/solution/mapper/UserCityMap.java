package uzum.solution.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import uzum.solution.dto.user.UserCityDTO;
import uzum.solution.entity.UserCities;

@Mapper(componentModel = "spring",
        uses = {
                UserMap.class,
                CityMap.class,
        })
public interface UserCityMap {

    @Mapping(target = "id", source = "id")
    @Mapping(target = "active", source = "active")
    @Mapping(target = "uuid", source = "uuid")
    @Mapping(target = "deleted", source = "deleted")
    @Mapping(target = "createdAt", source = "createdAt")
    @Mapping(target = "updatedAt", source = "updatedAt")
    @Mapping(target = "user", source = "user")
    @Mapping(target = "city", source = "city")
    UserCityDTO toDto(UserCities userCities);

    /*@Mapping(target = "id", ignore = true)
    @Mapping(target = "active", ignore = true)
    @Mapping(target = "uuid", ignore = true)
    @Mapping(target = "name", source = "name")
    @Mapping(target = "countryId", source = "countryId")
    UserCities toEntity(@MappingTarget City city, CityUpDTO dto);*/

}
