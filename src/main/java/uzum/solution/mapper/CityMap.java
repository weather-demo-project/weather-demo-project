package uzum.solution.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import uzum.solution.dto.city.CityDTO;
import uzum.solution.dto.city.CityUpDTO;
import uzum.solution.entity.City;

@Mapper(componentModel = "spring")
public interface CityMap {

    @Mapping(target = "id", source = "id")
    @Mapping(target = "active", source = "active")
    @Mapping(target = "uuid", source = "uuid")
    @Mapping(target = "deleted", source = "deleted")
    @Mapping(target = "createdAt", source = "createdAt")
    @Mapping(target = "updatedAt", source = "updatedAt")
    @Mapping(target = "name", source = "name")
    @Mapping(target = "country", source = "country")
    CityDTO toDto(City city);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "active", ignore = true)
    @Mapping(target = "uuid", ignore = true)
    @Mapping(target = "name", source = "name")
    @Mapping(target = "countryId", source = "countryId")
    City toEntity(@MappingTarget City city, CityUpDTO dto);

}
