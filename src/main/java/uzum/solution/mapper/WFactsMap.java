package uzum.solution.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import uzum.solution.dto.city.CityUpWeatherDTO;
import uzum.solution.dto.weather.WeatherDTO;
import uzum.solution.entity.WeatherFacts;

@Mapper(componentModel = "spring",
        uses = {
                CityMap.class
        })
public interface WFactsMap {

    @Mapping(target = "id", source = "id")
    @Mapping(target = "active", source = "active")
    @Mapping(target = "uuid", source = "uuid")
    @Mapping(target = "deleted", source = "deleted")
    @Mapping(target = "createdAt", source = "createdAt")
    @Mapping(target = "updatedAt", source = "updatedAt")
    @Mapping(target = "city", source = "city")
    @Mapping(target = "params", source = "params")
    @Mapping(target = "time", source = "time")
    WeatherDTO toDto(WeatherFacts weatherFacts);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "active", ignore = true)
    @Mapping(target = "uuid", ignore = true)
    @Mapping(target = "city", ignore = true)
    @Mapping(target = "paramsId", source = "paramsId")
    @Mapping(target = "timeId", source = "timeId")
    WeatherFacts toEntity(@MappingTarget WeatherFacts weatherFacts, CityUpWeatherDTO dto);

}
