package uzum.solution.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uzum.solution.base.BaseURI;
import uzum.solution.common.ResponseData;
import uzum.solution.dto.AttachRoleToUserDTO;
import uzum.solution.dto.GetAllUsersDTO;
import uzum.solution.dto.GetUserDTO;
import uzum.solution.dto.city.CityDTO;
import uzum.solution.dto.user.SubscribeToCityDTO;
import uzum.solution.dto.user.UserCityDTO;
import uzum.solution.dto.user.UserDTO;
import uzum.solution.dto.user.UserUpDTO;
import uzum.solution.entity.Role;
import uzum.solution.exception.UserNotFoundException;
import uzum.solution.service.UserServ;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(BaseURI.API + BaseURI.V1 + BaseURI.USER)
@Api(value = "USER APIS", description = "For the USER category")
public class UserController {

    private final UserServ serv;

    @ApiOperation(value = "Save Role")
    @PostMapping(BaseURI.ROLE + BaseURI.ADD)
    public ResponseEntity<Role> saveRole(@RequestBody Role role) {
        return new ResponseEntity<>(serv.saveRole(role), HttpStatus.CREATED);
    }

    @ApiOperation(value = "Attach Role to User")
    @PostMapping(BaseURI.ATTACH + BaseURI.ROLE + BaseURI.TO + BaseURI.USER)
    public ResponseEntity<?> attachRoleToUser(@RequestBody AttachRoleToUserDTO dto) {
        serv.attachRoleToUser(dto.getPhone(), dto.getRoleName());
        return ResponseEntity.ok().build();
    }

    @ApiOperation(value = "Get all USERS")
    @GetMapping(BaseURI.GET + BaseURI.ALL + BaseURI.USERS)
    public ResponseEntity<ResponseData<List<UserDTO>>> getUserList(@Valid @RequestBody GetAllUsersDTO dto) throws UserNotFoundException {
        return serv.getUserList(dto);
    }

    @ApiOperation(value = "Get USER by UUID")
    @GetMapping(BaseURI.GET + BaseURI.USER)
    public ResponseEntity<ResponseData<UserDTO>> getUserDetails(@Valid @RequestBody GetUserDTO dto) throws UserNotFoundException {
        return serv.getUserDetails(dto);
    }

    @ApiOperation(value = "Update USER")
    @PostMapping(BaseURI.EDIT + BaseURI.USER)
    public ResponseEntity<ResponseData<UserDTO>> editUser(@Valid @RequestBody UserUpDTO dto) {
        return serv.editUser(dto);
    }
}
