package uzum.solution.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uzum.solution.base.BaseURI;
import uzum.solution.common.ResponseData;
import uzum.solution.dto.city.CityDTO;
import uzum.solution.dto.city.CityUpDTO;
import uzum.solution.dto.city.CityUpWeatherDTO;
import uzum.solution.dto.user.SubscribeToCityDTO;
import uzum.solution.dto.user.UserCityDTO;
import uzum.solution.dto.weather.GetWeatherSubscribedCityDTO;
import uzum.solution.dto.weather.WeatherDTO;
import uzum.solution.service.CityServ;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(BaseURI.API + BaseURI.V1 + BaseURI.CITY)
@Api(value = "CITY APIS", description = "For the CITY category")
public class CityController {

    private final CityServ serv;

    @ApiOperation(value = "Get all CITIES")
    @GetMapping(BaseURI.GET + BaseURI.ALL + BaseURI.CITIES)
    public ResponseEntity<ResponseData<List<CityDTO>>> getCityList() {
        return serv.getCityList();
    }

    @ApiOperation(value = "Update CITY")
    @PostMapping(BaseURI.EDIT + BaseURI.CITY)
    public ResponseEntity<ResponseData<CityDTO>> editCity(@Valid @RequestBody CityUpDTO dto) {
        return serv.editCity(dto);
    }

    @ApiOperation(value = "Update CITY")
    @PostMapping(BaseURI.EDIT + BaseURI.CITY + BaseURI.WEATHER)
    public ResponseEntity<ResponseData<WeatherDTO>> editCityWeather(@Valid @RequestBody CityUpWeatherDTO dto) {
        return serv.editCityWeather(dto);
    }

    @ApiOperation(value = "Subscribe to CITY")
    @PostMapping(BaseURI.SUBSCRIBE + BaseURI.TO + BaseURI.CITY)
    public ResponseEntity<ResponseData<UserCityDTO>> subscribeCity(@Valid @RequestBody SubscribeToCityDTO dto) {
        return serv.subscribeCity(dto);
    }

    @ApiOperation(value = "Get Weather From Subscribed CITY")
    @GetMapping(BaseURI.GET + BaseURI.WEATHER + BaseURI.FROM + BaseURI.SUBSCRIBED + BaseURI.CITY)
    public ResponseEntity<ResponseData<WeatherDTO>> getWeatherFromSubscribedCity(@Valid @RequestBody GetWeatherSubscribedCityDTO dto) {
        return serv.getWeatherFromSubscribedCity(dto);
    }
}
