package uzum.solution.entity;

import lombok.*;
import uzum.solution.base.BaseEntity;

import javax.persistence.*;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "countries")
public class Country extends BaseEntity {

    @Column(name = "name")
    private String name;

}
