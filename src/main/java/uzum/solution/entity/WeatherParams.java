package uzum.solution.entity;

import lombok.*;
import uzum.solution.base.BaseEntity;

import javax.persistence.*;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "weather_parameters")
public class WeatherParams extends BaseEntity {

    @Column(name = "min")
    private String min;

    @Column(name = "max")
    private String max;

    @Column(name = "morn")
    private String morn;

    @Column(name = "day")
    private String day;

    @Column(name = "eve")
    private String eve;

    @Column(name = "night")
    private String night;

    @Column(name = "wind")
    private String wind;

    @Column(name = "humidity")
    private String humidity;

    @Column(name = "cloud")
    private String cloud;

    @Column(name = "rain")
    private String rain;
}
