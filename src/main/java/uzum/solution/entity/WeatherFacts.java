package uzum.solution.entity;

import lombok.*;
import uzum.solution.base.BaseEntity;

import javax.persistence.*;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "weather_facts")
public class WeatherFacts extends BaseEntity {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "city_id", referencedColumnName = "id", insertable = false, updatable = false)
    private City city;
    @Column(name = "city_id")
    private Long cityId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "params_id", referencedColumnName = "id", insertable = false, updatable = false)
    private WeatherParams params;
    @Column(name = "params_id")
    private Long paramsId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "time_id", referencedColumnName = "id", insertable = false, updatable = false)
    private TimeTable time;
    @Column(name = "time_id")
    private Long timeId;
}
