package uzum.solution.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AttachRoleToUserDTO {

    @JsonProperty("phone")
    private String phone;

    @JsonProperty("roleName")
    private String roleName;

}
