package uzum.solution.dto.auth;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ReqRefreshTokenDTO {

    private String refreshToken;

}
