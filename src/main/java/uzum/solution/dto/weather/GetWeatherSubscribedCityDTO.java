package uzum.solution.dto.weather;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class GetWeatherSubscribedCityDTO {

    @JsonProperty("user")
    private UUID user;

    @JsonProperty("city")
    private UUID city;

}
