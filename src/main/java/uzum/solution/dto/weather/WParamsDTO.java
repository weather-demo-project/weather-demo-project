package uzum.solution.dto.weather;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties({"id", "uuid", "active", "deleted", "createdAt", "updatedAt"})
public class WParamsDTO {

    @JsonProperty("id")
    private Long id;

    @JsonProperty("min")
    private String min;

    @JsonProperty("max")
    private String max;

    @JsonProperty("morn")
    private String morn;

    @JsonProperty("day")
    private String day;

    @JsonProperty("eve")
    private String eve;

    @JsonProperty("night")
    private String night;

    @JsonProperty("wind")
    private String wind;

    @JsonProperty("humidity")
    private String humidity;

    @JsonProperty("cloud")
    private String cloud;

    @JsonProperty("rain")
    private String rain;

    @JsonProperty("active")
    private boolean active;

    @JsonProperty("deleted")
    private boolean deleted;

    @JsonProperty("uuid")
    private UUID uuid;

    @JsonProperty("createdAt")
    protected LocalDateTime createdAt;

    @JsonProperty("updatedAt")
    protected LocalDateTime updatedAt;

}
