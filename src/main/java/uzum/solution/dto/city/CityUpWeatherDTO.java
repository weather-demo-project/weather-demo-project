package uzum.solution.dto.city;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.UUID;

@Getter
@Setter
@ToString
public class CityUpWeatherDTO {

    @JsonProperty("admin")
    private UUID admin;

    @JsonProperty("city")
    private UUID city;

    @JsonProperty("weatherFacts")
    private UUID weatherFacts;

    @JsonProperty("paramsId")
    private Long paramsId;

    @JsonProperty("timeId")
    private Long timeId;
}
