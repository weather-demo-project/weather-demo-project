package uzum.solution.dto.city;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.UUID;

@Getter
@Setter
@ToString
public class CityUpDTO {

    @JsonProperty("admin")
    private UUID admin;

    @JsonProperty("city")
    private UUID city;

    @JsonProperty("name")
    private String name;

    @JsonProperty("countryId")
    private Long countryId;
}
