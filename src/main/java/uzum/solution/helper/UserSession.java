package uzum.solution.helper;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import uzum.solution.entity.User;
import uzum.solution.exception.UserNotFoundException;
import uzum.solution.service.UserServ;

@Component
@RequiredArgsConstructor
public class UserSession {

    public final UserServ serv;

    public User getUser() throws UserNotFoundException {
        String username = SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
        return serv.findByUsername(username);
    }
}
