package uzum.solution.base;

public interface BaseURI {

    String API = "/api";
    String V1 = "/v1";

    String API1 = API + V1;

    String PUBLIC = "/public";

    String USER = "/user";
    String USERS = "/users";
    String CITY = "/city";
    String WEATHER = "/weather";
    String CITIES = "/cities";
    String ROLE = "/role";

    String REGISTRATION = "/registration";
    String LOGIN = "/login";
    String TOKEN = "/token";
    String REFRESH = "/refresh";
    String SUBSCRIBE = "/subscribe";
    String SUBSCRIBED = "/subscribed";
    String ATTACH = "/attach";
    String TO = "/to";
    String FROM = "/from";
    String GET = "/get";
    String ALL = "/all";
    String ADD = "/add";
    String EDIT = "/edit";

}
