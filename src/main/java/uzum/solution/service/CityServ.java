package uzum.solution.service;

import org.springframework.http.ResponseEntity;
import uzum.solution.common.ResponseData;
import uzum.solution.dto.city.CityDTO;
import uzum.solution.dto.city.CityUpDTO;
import uzum.solution.dto.city.CityUpWeatherDTO;
import uzum.solution.dto.user.SubscribeToCityDTO;
import uzum.solution.dto.user.UserCityDTO;
import uzum.solution.dto.weather.GetWeatherSubscribedCityDTO;
import uzum.solution.dto.weather.WeatherDTO;
import uzum.solution.entity.City;
import uzum.solution.entity.TimeTable;
import uzum.solution.entity.WeatherFacts;
import uzum.solution.entity.WeatherParams;

import java.util.List;
import java.util.UUID;

public interface CityServ {

    ResponseEntity<ResponseData<List<CityDTO>>> getCityList();

    ResponseEntity<ResponseData<CityDTO>> editCity(CityUpDTO dto);

    ResponseEntity<ResponseData<WeatherDTO>> editCityWeather(CityUpWeatherDTO dto);

    ResponseEntity<ResponseData<UserCityDTO>> subscribeCity(SubscribeToCityDTO dto);

    ResponseEntity<ResponseData<WeatherDTO>> getWeatherFromSubscribedCity(GetWeatherSubscribedCityDTO dto);

    City checkCity(UUID uuid);

    WeatherFacts checkWeatherFacts(UUID uuid);

    WeatherParams checkWeatherParams(Long id);

    TimeTable checkTimeTable(Long id);


}
