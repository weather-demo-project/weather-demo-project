package uzum.solution.service.impl;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import uzum.solution.common.ResponseData;
import uzum.solution.dto.GetAllUsersDTO;
import uzum.solution.dto.GetUserDTO;
import uzum.solution.dto.auth.ReqLoginDTO;
import uzum.solution.dto.auth.ReqRefreshTokenDTO;
import uzum.solution.dto.auth.ReqRegDTO;
import uzum.solution.dto.auth.SessionDTO;
import uzum.solution.dto.user.UserDTO;
import uzum.solution.dto.user.UserUpDTO;
import uzum.solution.entity.Role;
import uzum.solution.entity.User;
import uzum.solution.enums.RoleType;
import uzum.solution.exception.AlreadyExistsException;
import uzum.solution.exception.UserNotFoundException;
import uzum.solution.helper.Utils;
import uzum.solution.mapper.UserMap;
import uzum.solution.repo.RoleRepo;
import uzum.solution.repo.UserRepo;
import uzum.solution.service.UserServ;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.io.IOException;
import java.net.URI;
import java.util.*;

@Service
@Slf4j
@RequiredArgsConstructor
public class UserServImpl implements UserServ {

    private final UserRepo repo;
    private final RoleRepo roleRepo;
    private final PasswordEncoder passwordEncoder;
    private final UserMap mapper;

    @Override
    public ResponseEntity<ResponseData<List<UserDTO>>> getUserList(GetAllUsersDTO dto) throws UserNotFoundException {
        User users = this.checkUser(dto.getAdmin()); // check and get user
        if (Utils.isEmpty(users)) {
            log.error("User uuid, {} bo'yicha ma'lumot topilmadi", this.checkUser(dto.getAdmin()));
            return ResponseData.notFoundData("User is not found !!!");
        } else if (!users.isActive()) {
            log.error("User uuid, {} bo'yicha faol emas!", this.checkUser(dto.getAdmin()));
            return ResponseData.inActive("This User's status is inactive !!!");
        }

        for (Role role : users.getRoles()) {
            if (Utils.isAdmin(role.getName())) {
                List<User> list = repo.findAll();
                if (list.isEmpty()) {
                    log.warn("Foydalanuvchilar ro'yxati topilmadi!");
                    return ResponseData.notFoundData("Users are not found !!!");
                }
                List<UserDTO> dtoList = new ArrayList<>();
                list.forEach(user -> dtoList.add(mapper.toDto(user)));
                log.info("Barcha foydalanuvchilar ro'yxati olindi");
                return ResponseData.success200(dtoList);
            } else {
                log.warn("Sizda adminlik huquqi yo'q !!!");
                return ResponseData.notFoundData("You do not have admin rights !!!");
            }
        }
        log.warn("Foydalanuvchi roli aniqlanmadi !!!");
        return ResponseData.notFoundData("User role not defined !!!");
    }

    @Override
    public ResponseEntity<ResponseData<UserDTO>> getUserDetails(GetUserDTO dto) throws UserNotFoundException {
        User users = this.checkUser(dto.getAdmin()); // check and get user

        for (Role role : users.getRoles()) {
            if (Utils.isAdmin(role.getName())) {
                Optional<User> user = repo.findByUuid(dto.getUser());
                if (Utils.isEmpty(user)) {
                    log.error("User uuid, {} bo'yicha ma'lumot topilmadi", dto.getUser());
                    ResponseData.notFoundData("User not found !!!");
                }
                log.info("User uuid, {} bo'yicha ma'lumot olindi", dto.getUser());
                return ResponseData.success200(mapper.toDto(user.get()));
            } else {
                log.warn("Sizda adminlik huquqi yo'q !!!");
                return ResponseData.notFoundData("You do not have admin rights !!!");
            }
        }
        log.warn("Foydalanuvchi roli aniqlanmadi !!!");
        return ResponseData.notFoundData("User role not defined !!!");
    }

    @Transactional
    @Override
    public ResponseEntity<ResponseData<UserDTO>> editUser(UserUpDTO dto) {
        User users = this.checkUser(dto.getAdmin());

        for (Role role : users.getRoles()) {
            if (Utils.isAdmin(role.getName())) {
                Optional<User> optional = repo.findByUuid(dto.getUser());
                if (optional.isEmpty()) {
                    log.error("User uuid, {} bo'yicha ma'lumot topilmadi", dto.getUser());
                    return ResponseData.notFoundData("User not found !!!");
                }
                User user = optional.get();
                if (!user.isActive()) {
                    log.error("User uuid, {} bo'yicha faol emas!", dto.getUser());
                    return ResponseData.inActive("This user is not active !!!");
                }
                user = mapper.toEntity(user, dto);
                repo.save(user);
                log.info("User {} - ma'lumotlari yangilandi!", user.getFirstName());
                return ResponseData.success202(mapper.toDto(user));
            } else {
                log.warn("Sizda adminlik huquqi yo'q !!!");
                return ResponseData.notFoundData("You do not have admin rights !!!");
            }
        }
        log.warn("Foydalanuvchi roli aniqlanmadi !!!");
        return ResponseData.notFoundData("User role not defined !!!");
    }

    @Transactional
    @Override
    public Role saveRole(Role role) {
        log.info("Yangi role - {} saqlandi", role.getName());
        return roleRepo.save(role);
    }

    @Transactional
    @Override
    public void attachRoleToUser(String phone, String roleName) {
        log.info("User {} ga {} - role biriktirildi!", phone, roleName);
        Optional<User> optional = repo.findByPhone(phone);
        User user = optional.get();
        Role role = roleRepo.findByName(roleName);
        user.getRoles().add(role);
        repo.save(user);
    }

    @Transactional
    @Override
    public ResponseEntity<ResponseData<UserDTO>> registration(ReqRegDTO req, HttpServletRequest httpReq) throws AlreadyExistsException {
        String roleName = RoleType.ROLE_USER.getDescription();

        Optional<User> optional = repo.findByPhone(req.getPhone());
        if (optional.isPresent()) {
            log.error("This user is already registered!");
            throw new AlreadyExistsException("This user is already registered!");
        }

        Role role = roleRepo.findByName(roleName);
        User user = mapper.toEntity(req);
        user.getRoles().add(role);
        user.setPassword(passwordEncoder.encode(req.getPassword()));
        repo.save(user);
        log.info("Yangi user - {} saqlandi", user.getFirstName());
        return ResponseData.success201(mapper.toDto(user));
    }

    @Override
    public ResponseEntity<SessionDTO> login(ReqLoginDTO req, HttpServletRequest httpReq) {
        if (Utils.isEmpty(req.getUsername())) {
            log.error("Username is required field!");
            throw new RuntimeException("Username is required field!");
        }
        if (Utils.isEmpty(req.getPassword())) {
            log.error("Password is required field!");
            throw new RuntimeException("Password is required field!");
        }

        try {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("username", req.getUsername()));
            nameValuePairs.add(new BasicNameValuePair("password", req.getPassword()));

            URI uri = URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path("/login").toUriString());

            HttpPost httpPost = new HttpPost(uri);
            httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            httpPost.addHeader("Content-Type", "application/x-www-form-urlencoded");

            HttpClient httpClient = HttpClientBuilder.create().build();
            HttpResponse response = httpClient.execute(httpPost);
            return getSessionDto(req, response);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("User username, {} bo'yicha ma'lumot topilmadi", req.getUsername());
            throw new RuntimeException("User not found !!!");
        }
    }

    @Transactional
    @Override
    public ResponseEntity<SessionDTO> refreshToken(ReqRefreshTokenDTO req, HttpServletRequest httpReq) {
        if (Utils.isEmpty(req.getRefreshToken())) {
            throw new RuntimeException("Refresh token is missing");
        }
        try {
            String refresh_token = req.getRefreshToken();
            Algorithm algorithm = Algorithm.HMAC256("trello".getBytes());
            JWTVerifier verifier = JWT.require(algorithm).build();
            DecodedJWT decodedJWT = verifier.verify(refresh_token);
            String username = decodedJWT.getSubject();
            Optional<User> user = repo.findByUsername(username);
            long expireIn = 60 * 60 * 1000;  // todo o'zgartirish mumkin 1 minutes
            String access_token = JWT.create()
                    .withSubject(user.get().getUsername())
                    .withExpiresAt(new Date(System.currentTimeMillis() + expireIn))   // 1 minutes
                    .withIssuer(httpReq.getRequestURL().toString())
                    .withClaim("roles", new ArrayList<>())  // todo set user roles
                    .sign(algorithm);
            SessionDTO sessionDto = SessionDTO.builder()
                    .expireIn(expireIn)
                    .accessToken(access_token)
                    .refreshToken(refresh_token)
                    .user(user.get())
                    .build();
            return ResponseEntity.ok(sessionDto);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private ResponseEntity<SessionDTO> getSessionDto(ReqLoginDTO req, HttpResponse response) throws IOException {
        JsonNode json_auth = new ObjectMapper().readTree(EntityUtils.toString(response.getEntity()));
        if (!json_auth.has("error") && !json_auth.has("detail_message")) {
            SessionDTO sessionDto = SessionDTO.builder()
                    .expireIn(json_auth.get("expires_in").asLong())
                    .accessToken(json_auth.get("access_token").asText())
                    .refreshToken(json_auth.get("refresh_token").asText())
                    .user(repo.findByUsername(req.getUsername()).get())
                    .build();
            return ResponseEntity.ok(sessionDto);
        } else {
            String error_message = "";
            if (json_auth.has("error")) {
                error_message = json_auth.get("error").asText();
            } else if (json_auth.has("detail_message")) {
                error_message = json_auth.get("detail_message").asText();
            }
            throw new RuntimeException(error_message);
        }
    }

    @Override
    public User findByUsername(String username) throws UsernameNotFoundException {
        Optional<User> userOptional = repo.findByUsername(username);
        if (userOptional.isEmpty()) {
            log.error("User username, {} bo'yicha ma'lumot topilmadi", username);
            throw new UsernameNotFoundException("User is not found !!!");
        }
        return userOptional.get();
    }

    @Override
    public User checkUser(UUID uuid) {
        Optional<User> optional = repo.findByUuid(uuid);
        if (optional.isEmpty()) {
            log.error("User uuid, {} bo'yicha ma'lumot topilmadi", uuid);
            throw new RuntimeException("User not found !!!");
        }
        User user = optional.get();
        if (!user.isActive()) {
            log.error("User uuid, {} bo'yicha faol emas!", uuid);
            throw new RuntimeException("This user is not active !!!");
        }
        return user;
    }

}
