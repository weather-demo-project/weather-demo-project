package uzum.solution.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uzum.solution.common.ResponseData;
import uzum.solution.dto.city.CityDTO;
import uzum.solution.dto.city.CityUpDTO;
import uzum.solution.dto.city.CityUpWeatherDTO;
import uzum.solution.dto.user.SubscribeToCityDTO;
import uzum.solution.dto.user.UserCityDTO;
import uzum.solution.dto.weather.GetWeatherSubscribedCityDTO;
import uzum.solution.dto.weather.WeatherDTO;
import uzum.solution.entity.*;
import uzum.solution.helper.Utils;
import uzum.solution.mapper.CityMap;
import uzum.solution.mapper.UserCityMap;
import uzum.solution.mapper.WFactsMap;
import uzum.solution.repo.*;
import uzum.solution.service.CityServ;
import uzum.solution.service.UserServ;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@Slf4j
@RequiredArgsConstructor
public class CityServImpl implements CityServ {

    private final CityRepo repo;
    private final WFactsRepo wFactsRepo;
    private final WParamsRepo wParamsRepo;
    private final TimeTableRepo tTableRepo;
    private final CityMap mapper;
    private final WFactsMap wFactsMapper;
    private final UserServ userServ;
    private final UserCityMap userCityMap;
    private final UserCitiesRepo userCitiesRepo;

    @Override
    public ResponseEntity<ResponseData<List<CityDTO>>> getCityList() {
        List<City> list = repo.findAll();
        if (list.isEmpty()) {
            log.warn("Shaharlar ro'yxati topilmadi!");
            return ResponseData.notFoundData("Cities are not found !!!");
        }
        List<CityDTO> dtoList = new ArrayList<>();
        list.forEach(city -> dtoList.add(mapper.toDto(city)));
        log.info("Barcha shaharlar ro'yxati olindi");
        return ResponseData.success200(dtoList);
    }

    @Transactional
    @Override
    public ResponseEntity<ResponseData<CityDTO>> editCity(CityUpDTO dto) {
        User users = userServ.checkUser(dto.getAdmin());

        for (Role role : users.getRoles()) {
            if (Utils.isAdmin(role.getName())) {
                Optional<City> optional = repo.findByUuid(dto.getCity());
                if (optional.isEmpty()) {
                    log.error("City uuid, {} bo'yicha ma'lumot topilmadi", dto.getCity());
                    return ResponseData.notFoundData("City not found !!!");
                }
                City city = optional.get();
                if (!city.isActive()) {
                    log.error("City uuid, {} bo'yicha faol emas!", dto.getCity());
                    return ResponseData.inActive("This city is not active !!!");
                }
                city = mapper.toEntity(city, dto);
                repo.save(city);
                log.info("City {} - ma'lumotlari yangilandi!", city.getName());
                return ResponseData.success202(mapper.toDto(city));
            } else {
                log.warn("Sizda adminlik huquqi yo'q !!!");
                return ResponseData.notFoundData("You do not have admin rights !!!");
            }
        }
        log.warn("Foydalanuvchi roli aniqlanmadi !!!");
        return ResponseData.notFoundData("User role not defined !!!");
    }

    @Transactional
    @Override
    public ResponseEntity<ResponseData<WeatherDTO>> editCityWeather(CityUpWeatherDTO dto) {

        User users = userServ.checkUser(dto.getAdmin());
        City cities = this.checkCity(dto.getCity());
        WeatherFacts weatherFacts = this.checkWeatherFacts(dto.getWeatherFacts());

        /*WeatherParams weatherParams = this.checkWeatherParams(dto.getParamsId());
        TimeTable timeTable = this.checkTimeTable(dto.getTimeId());*/

        for (Role role : users.getRoles()) {
            if (Utils.isAdmin(role.getName())) {
                if (Utils.isEmpty(cities)) {
                    log.error("City uuid, {} bo'yicha ma'lumot topilmadi", dto.getCity());
                    return ResponseData.notFoundData("City not found !!!");
                }
                if (!cities.isActive()) {
                    log.error("City uuid, {} bo'yicha faol emas!", dto.getCity());
                    return ResponseData.inActive("This city is not active !!!");
                }
                /*weatherFacts.setParamsId(weatherParams.getId());
                weatherFacts.setTimeId(timeTable.getId());*/
                weatherFacts = wFactsMapper.toEntity(weatherFacts, dto);
                wFactsRepo.save(weatherFacts);
                log.info("WeatherFacts id {} - ma'lumotlari yangilandi!", weatherFacts.getId());
                return ResponseData.success202(wFactsMapper.toDto(weatherFacts));
            }
        }
        log.warn("Foydalanuvchi roli aniqlanmadi !!!");
        return ResponseData.notFoundData("User role not defined !!!");
    }

    @Transactional
    @Override
    public ResponseEntity<ResponseData<UserCityDTO>> subscribeCity(SubscribeToCityDTO dto) {
        User user = userServ.checkUser(dto.getUser());
        City city = this.checkCity(dto.getCity());

        if (Utils.isEmpty(user)) {
            log.error("User uuid, {} bo'yicha ma'lumot topilmadi", dto.getUser());
            return ResponseData.notFoundData("User not found !!!");
        }
        if (!user.isActive()) {
            log.error("User uuid, {} bo'yicha faol emas!", dto.getUser());
            return ResponseData.inActive("This user is not active !!!");
        }

        if (Utils.isEmpty(city)) {
            log.error("City uuid, {} bo'yicha ma'lumot topilmadi", dto.getCity());
            return ResponseData.notFoundData("City not found !!!");
        }
        if (!city.isActive()) {
            log.error("City uuid, {} bo'yicha faol emas!", dto.getCity());
            return ResponseData.inActive("This city is not active !!!");
        }

        UserCities userCities = new UserCities();
        userCities.setUserId(user.getId());
        userCities.setUser(user);
        userCities.setCityId(city.getId());
        userCities.setCity(city);

        userCitiesRepo.save(userCities);
        log.error("User uuid {} ga, city uuid {} bo'yicha ulandi!", dto.getUser(), dto.getCity());

        return ResponseData.success202(userCityMap.toDto(userCities));
    }

    @Override
    public ResponseEntity<ResponseData<WeatherDTO>> getWeatherFromSubscribedCity(GetWeatherSubscribedCityDTO dto) {

        User user = userServ.checkUser(dto.getUser());
        City city = this.checkCity(dto.getCity());

        if (Utils.isEmpty(user)) {
            log.error("User uuid, {} bo'yicha ma'lumot topilmadi", dto.getUser());
            return ResponseData.notFoundData("User not found !!!");
        }
        if (!user.isActive()) {
            log.error("User uuid, {} bo'yicha faol emas!", dto.getUser());
            return ResponseData.inActive("This user is not active !!!");
        }

        if (Utils.isEmpty(city)) {
            log.error("City uuid, {} bo'yicha ma'lumot topilmadi", dto.getCity());
            return ResponseData.notFoundData("City not found !!!");
        }
        if (!city.isActive()) {
            log.error("City uuid, {} bo'yicha faol emas!", dto.getCity());
            return ResponseData.inActive("This city is not active !!!");
        }

        UserCities userCities = userCitiesRepo.findByUserUuidAndCityUuid(user.getUuid(), city.getUuid());

        if (Utils.isEmpty(userCities)) {
            log.error("User Cities uuid {},bo'yicha ma'lumot topilmadi", userCities.getUuid());
            return ResponseData.notFoundData("User not found !!!");
        }
        if (!userCities.isActive()) {
            log.error("User Cities, {} bo'yicha faol emas!", userCities.getUuid());
            return ResponseData.inActive("This user is not active !!!");
        }

        Optional<WeatherFacts> optional = wFactsRepo.findByCityUuid(userCities.getCity().getUuid());
        WeatherFacts weatherFacts = optional.get();

        return ResponseData.success200(wFactsMapper.toDto(weatherFacts));
    }

    @Override
    public City checkCity(UUID uuid) {
        Optional<City> optional = repo.findByUuid(uuid);
        if (optional.isEmpty()) {
            log.error("City uuid, {} bo'yicha ma'lumot topilmadi", uuid);
            throw new RuntimeException("City not found !!!");
        }
        City city = optional.get();
        if (!city.isActive()) {
            log.error("City uuid, {} bo'yicha faol emas!", uuid);
            throw new RuntimeException("This city is not active !!!");
        }
        return city;
    }

    @Override
    public WeatherFacts checkWeatherFacts(UUID uuid) {
        Optional<WeatherFacts> optional = wFactsRepo.findByUuid(uuid);
        if (optional.isEmpty()) {
            log.error("WeatherFacts uuid, {} bo'yicha ma'lumot topilmadi", uuid);
            throw new RuntimeException("WeatherFacts not found !!!");
        }
        WeatherFacts weatherFacts = optional.get();
        if (!weatherFacts.isActive()) {
            log.error("WeatherFacts uuid, {} bo'yicha faol emas!", uuid);
            throw new RuntimeException("This weatherFacts is not active !!!");
        }
        return weatherFacts;
    }

    @Override
    public WeatherParams checkWeatherParams(Long id) {
        Optional<WeatherParams> optional = wParamsRepo.findById(id);
        if (optional.isEmpty()) {
            log.error("WeatherParams id, {} bo'yicha ma'lumot topilmadi", id);
            throw new RuntimeException("WeatherParams not found !!!");
        }
        WeatherParams weatherParams = optional.get();
        if (!weatherParams.isActive()) {
            log.error("WeatherParams id, {} bo'yicha faol emas!", id);
            throw new RuntimeException("This weatherParams is not active !!!");
        }
        return weatherParams;
    }

    @Override
    public TimeTable checkTimeTable(Long id) {
        Optional<TimeTable> optional = tTableRepo.findById(id);
        if (optional.isEmpty()) {
            log.error("TimeTable id, {} bo'yicha ma'lumot topilmadi", id);
            throw new RuntimeException("TimeTable not found !!!");
        }
        TimeTable timeTable = optional.get();
        if (!timeTable.isActive()) {
            log.error("TimeTable id, {} bo'yicha faol emas!", id);
            throw new RuntimeException("This timeTable is not active !!!");
        }
        return timeTable;
    }
}
