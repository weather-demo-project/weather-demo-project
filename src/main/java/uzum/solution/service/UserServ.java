package uzum.solution.service;

import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import uzum.solution.common.ResponseData;
import uzum.solution.dto.GetAllUsersDTO;
import uzum.solution.dto.GetUserDTO;
import uzum.solution.dto.auth.ReqLoginDTO;
import uzum.solution.dto.auth.ReqRefreshTokenDTO;
import uzum.solution.dto.auth.ReqRegDTO;
import uzum.solution.dto.auth.SessionDTO;
import uzum.solution.dto.city.CityDTO;
import uzum.solution.dto.city.CityUpDTO;
import uzum.solution.dto.user.*;
import uzum.solution.entity.Role;
import uzum.solution.entity.User;
import uzum.solution.exception.AlreadyExistsException;
import uzum.solution.exception.UserNotFoundException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.UUID;

public interface UserServ {
    ResponseEntity<ResponseData<List<UserDTO>>> getUserList(GetAllUsersDTO dto) throws UserNotFoundException;

    ResponseEntity<ResponseData<UserDTO>> getUserDetails(GetUserDTO dto) throws UserNotFoundException;

    ResponseEntity<ResponseData<UserDTO>> editUser(UserUpDTO dto);

    ResponseEntity<ResponseData<UserDTO>> registration(ReqRegDTO req, HttpServletRequest httpReq) throws AlreadyExistsException;

    ResponseEntity<SessionDTO> login(ReqLoginDTO req, HttpServletRequest httpReq);

    ResponseEntity<SessionDTO> refreshToken(ReqRefreshTokenDTO req, HttpServletRequest httpReq);

    Role saveRole(Role role);

    void attachRoleToUser(String phone, String roleName);

    User findByUsername(String username) throws UsernameNotFoundException;

    User checkUser(UUID uuid);


}
