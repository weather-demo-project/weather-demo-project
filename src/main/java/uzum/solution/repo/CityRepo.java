package uzum.solution.repo;

import org.springframework.stereotype.Repository;
import uzum.solution.base.BaseRepo;
import uzum.solution.entity.City;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface CityRepo extends BaseRepo<City> {
    Optional<City> findByUuid(UUID city);
}
