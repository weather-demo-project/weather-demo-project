package uzum.solution.repo;

import org.springframework.stereotype.Repository;
import uzum.solution.base.BaseRepo;
import uzum.solution.entity.UserCities;

import java.util.UUID;

@Repository
public interface UserCitiesRepo extends BaseRepo<UserCities> {
    UserCities findByUuid(UUID uuid);

    UserCities findByUserUuidAndCityUuid(UUID user, UUID city);
}
