package uzum.solution.repo;

import org.springframework.stereotype.Repository;
import uzum.solution.base.BaseRepo;
import uzum.solution.entity.TimeTable;
import uzum.solution.entity.WeatherParams;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface TimeTableRepo extends BaseRepo<TimeTable> {
    Optional<TimeTable> findByUuid(UUID uuid);
    Optional<TimeTable> findById(Long id);
}
