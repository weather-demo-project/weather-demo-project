package uzum.solution.repo;

import org.springframework.stereotype.Repository;
import uzum.solution.base.BaseRepo;
import uzum.solution.entity.Role;

@Repository
public interface RoleRepo extends BaseRepo<Role> {

    Role findByName(String name);

}
