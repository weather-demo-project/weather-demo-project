package uzum.solution.repo;

import org.springframework.stereotype.Repository;
import uzum.solution.base.BaseRepo;
import uzum.solution.entity.WeatherParams;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface WParamsRepo extends BaseRepo<WeatherParams> {
    Optional<WeatherParams> findByUuid(UUID uuid);

    Optional<WeatherParams> findById(Long id);
}
