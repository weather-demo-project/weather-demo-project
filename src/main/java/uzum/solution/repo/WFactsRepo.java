package uzum.solution.repo;

import org.springframework.stereotype.Repository;
import uzum.solution.base.BaseRepo;
import uzum.solution.entity.City;
import uzum.solution.entity.WeatherFacts;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface WFactsRepo extends BaseRepo<WeatherFacts> {
    Optional<WeatherFacts> findByUuid(UUID uuid);
    Optional<WeatherFacts> findByCityUuid(UUID uuid);
}
