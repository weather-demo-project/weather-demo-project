package uzum.solution.repo;

import org.springframework.stereotype.Repository;
import uzum.solution.base.BaseRepo;
import uzum.solution.entity.User;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface UserRepo extends BaseRepo<User> {

    Optional<User> findByUuid(UUID uuid);

    List<User> findAllByDeletedIsFalseAndActiveIsTrue();

    Optional<User> findByPhone(String phone);
    Optional<User> findByUsername(String username);
}
